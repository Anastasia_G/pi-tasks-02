#define _CRT_SECURE_NO_WARNINGS 
#define N 40 
#define K 2 
#include <stdio.h> 
#include <string.h> 
#include <time.h> 
int main() 
{ 
	int i, j, titleLetters, uppercaseLetters,figures, takt; 
	int size; 
	char s[256]; 
	printf("Enter size of password: "); 
	scanf("%i", &size); 
	srand(time(0)); 
	for (i = 0; i < N; ++i) 
	{ 
		titleLetters = 0; uppercaseLetters = 0; figures = 0; 
		while (titleLetters == 0 || uppercaseLetters == 0 || figures == 0) 
		{ 
			titleLetters = 0; uppercaseLetters = 0; figures = 0; 
			for (j=0; j<size; ++j) 
			{ 
				takt = rand() % 3; 
				if (takt == 0) 
				{ 
					s[j] = rand() %('Z'-'A'+1)+'A'; 
					titleLetters = 1; 
				} 
				if (takt == 1) 
				{ 
					s[j] = rand() % ('z'-'a'+1)+'a'; 
					uppercaseLetters = 1; 
				} 
				if (takt == 2) 
				{ 
					s[j] = rand() %('9'-'0'+1)+'0'; 
					figures = 1; 
				} 
			} 
		} 
		for (j = 0; j < size; ++j) 
		printf("%c", s[j]); 
		if ((i + 1) % K == 0) 
		printf("\n"); 
		else 
		printf(" "); 
	} 
	return 0; 
}